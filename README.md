# Toolkit - Specifications and Examples

## BDD - Behaviour-driven Development

BDD is a results-oriented, purpose-driven workflow designed to reduce time wasted in software projects.

Its key purpose is to encourage communication and collaboration between clients, product owners, project managers, domain/business experts, business analysts, UX designers, developers, test engineers, DevOps and users.

Valuable to define:

-   how a part of the system should behave when it's working
-   examples to determine if the system actually behaves that way
-   the mechanism to verify that examples match the behaviour in a given environment

Remaining focussed on client or business outcomes - examples, rules

## Toolkit

-   Start with a description of an outcome

    -   descriptions of features
    -   user stories
    -   examples

-   _Cucumber_ is a collection of open-source tools that helps teams to feed examples into the development pipeline and to programatically determine if those examples behave as expected

    -   part of the `Cucumber` stack is the `Gherkin` language
        -   loosely structured, without many symbols
        -   mostly natural language

-   Example Mapping
    -   https://cucumber.io/blog/example-mapping-introduction/

# Recommended Visual Studio Code extension

Cucumber (Gherkin) Full Support
https://marketplace.visualstudio.com/items?itemName=alexkrechik.cucumberautocomplete

# Cypres + Cucumber

-   tags
-   smart tag: `@focus`

# Bravo Examples

## Redirects

-   Kustomizor
    -   golive example: `smoketests .. YHI\Kustomizor\redirects.feature`
    -   add `@focus`
-   City Recital Hall
    -   Error pages: `smoketests .. CRH\smoketest.feature`
    -   404, 500, 400
-   Melbourne Festival
    -   Redirect change, status codes: `inspect .. Melbourne Festival\config-redirects.feature`

## Flexibility: semi-manual tests

-   load project environments
    -   plan and organise URIs
    -   login
    -   navigation
-   load pages to investigate issues
-   automating tests over time

### Testable Targets

-   ids? classes? selectors?
-   data attributes
    -   `data-test` - known by test automation tools
    -   defined as part of design/spec, before development begins

### Config Pages

-   extending existing systems: navigation

    -   Example: `inspect .. YHI\task\freight calc\freight-calc-config.feature`

### Adelaide Festival

-   manual tests:

    -   navigation
        -   snapshot redirects, 3xx etc
    -   DOM snapshots
    -   XHR snapshots

    -   Example: driving Chrome, no tests `inspect\ .. Adelaide\issue-umbraco-forms.feature`

    -   Break the current uri out into a new tab for manual exploration at any time

### Complex Scenarios

-   manual sections: continue after manual actions

    -   Example: `yhi` - `freight-calc-examples`

# Cypress Routes

-   save fixtures from API calls
-   stub - like mocking
    -   Example: `yhi` - `freight-calc-examples`

## XHR snapshots

-   request, body, XHR object
    -   Example: `yhi` - `freight-calc-examples`
-   aliases

## Waiting for asynchronous calls

-   click a button
    -   update shopping cart
-   wait for ajax call to complete
    -   wait for @update cart ajax
